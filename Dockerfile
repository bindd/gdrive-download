FROM node:13-slim

COPY src/ /app

WORKDIR /app

RUN mkdir /dist && chmod 777 /dist

RUN yarn install --production

ENTRYPOINT yarn run drive
