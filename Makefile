#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
DATE=$(shell LC_ALL=fr_FR.utf8 date +'%_d %B %Y')
export DATE
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

# command name that are also directories
.PHONY: dev

#-----------------------------------------
# Allow passing arguments to make
#-----------------------------------------
SUPPORTED_COMMANDS := yarn vue
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(COMMAND_ARGS):;@:)
endif

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY: help
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#-----------------------------------------
# Commands
#-----------------------------------------
yarn: ## Runs yarn <anything>
	docker-compose run --rm build yarn ${COMMAND_ARGS}

connect: ## Runs node in docker with interactive shell
	@docker-compose run --rm build bash

build: ## Build docker image
	@docker-compose build --pull gdrive

test: build ## Build and test
	@rm -rf ./dist
	@mkdir -p ./dist
	@docker-compose run --rm gdrive
